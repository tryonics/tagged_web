<?php
return array(

		/*
		|--------------------------------------------------------------------------
		| Common list limit
		|--------------------------------------------------------------------------
		| This parameter will be used as the limit for all list apear in the api requests. 
		| For special cases we need to intruduce new veriables
		|
		*/
		'list_limit' =>2
);