<?php

class Api_V1_PromotionController extends \BaseController {

	
/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$promotion_lib = new PromotionLib();
		$lat = Input::get('latitude');
		$lon = Input::get('longitude');
		$offset = (int)Input::get('offset');
		$limit = Config::get("api.list_limit");
		$result = array(); // this will keep the data set ro return
		$error_obj = array(); // this will keep the error status and messages
		
		// get the promotion list
		$promotion_list = $promotion_lib->get_promotion_list($lat,$lon,$limit,$offset);
		
		if(!empty($promotion_list['records']))
		{
			foreach ($promotion_list['records'] as $promotion)
			{
				// format the promotion list to give the response
				$result[] = $promotion_lib->create_promotion_element($promotion);
			}
			//set the error object status false
			$error_obj = array("status"=> false,"message"=>"");
		}
		else // set empty message
		{
			$error_obj = array("status"=> true,"message"=>"empty results"); //TODO: need to get proper copy text
		}
		
		
		
		return Response::json(array(
				'error' => $error_obj,
				'prromotions' => $result,
				'offset'=>$promotion_list['offset'],
			), 200
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		echo "this is to test created crontab";//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$error_obj = array("status"=> false,"message"=>"");
		$result = array(
							'title' 		=> "Test_product",
							'description'	=> 'test description',
							'expiry_date'	=> 'Offer valid till 10th July, 2014',
							'image'			=> 'http://static.businessinsider.com/image/5228d9db6bb3f75f278b4567/image.jpg',
							'sub_content'	=> array(
												array('title' => 'POPULAR FLAVOURS','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis'),
												array('title' =>'NEWEST ADDITION','description'=>'test newest aaddition description')
							),
					);
		return Response::json(array(
				'error' => $error_obj,
				'prromotion' => $result
			), 200
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
