<?php

class Api_V1_EventController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$offset = (int)Input::get('offset');
		$limit = 1;//Config::get("api.list_limit");
		
		$result = array(); // this will keep the data set ro return
		$error_obj = array(); // this will keep the error status and messages
		
		$event_lib = new EventLib();
		$events = $event_lib->get_event_list($limit,$offset);
		if(!empty($events['records']))
		{
			foreach ($events['records'] as $event)
			{
				// format the event list to give the response
				$result[] = $event_lib->create_event_element($event);
			}
			//set the error object status false
			$error_obj = array("status"=> false,"message"=>"");
		}
		
		return Response::json(
			array(
				'error' => $error_obj,
				'events' => $result,
				'offset'=>$events['offset'],
			), 200
		);
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
