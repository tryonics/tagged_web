<?php
class EventModel extends Eloquent
{
	protected $table = "event";
	
	/*
	 * This function will list the events
	 */
	public function get_event_list($limit=0,$offset=0)
	{
		
		$result =  DB::table('event')
		->leftJoin('user', function($join)
        {
            $join->on('event.user_id', '=', 'user.user_id')
            ->where('user.active', '=', 1);
        })
		->select('event.*', 'user.first_name')
		->where('event.active','=',1)
		->where(function($query){
			//$query->where('event.start_date','=',0);
			$query->orWhere(function($sub_query){
				$current_time = time();
				//$sub_query->where('event.start_date',">=",$current_time);
			});
			
		});
		if($limit)
		{
			$result = $result->skip($offset)->take($limit);
		}
		$result = $result->get();
		return $result;
	}
}