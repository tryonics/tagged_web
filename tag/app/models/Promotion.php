<?php
class Promotion extends Eloquent
{
	protected $table="promotion";
	/*
	 * This function will list the promotions
	 */
	public function list_promotions($lat=0,$lon=0,$limit=0,$offset=0,$order_by)
	{
		$current_time = time();
		$results = array();
		$results['records'] = DB::select( DB::raw('
				SELECT SQL_CALC_FOUND_ROWS (tag_promotion.promotion_id),`tag_promotion`.*, tag_distance(tag_location.Lat,tag_location.Lon,'.$lat.','.$lon.',"") AS distance 
				FROM `tag_promotion` 
				LEFT JOIN `tag_user` ON `tag_promotion`.`user_id` = `tag_user`.`user_id` and `tag_user`.`active` = 1 
				LEFT JOIN `tag_promotion_location` ON `tag_promotion`.`promotion_id` = `tag_promotion_location`.`promotion_id` AND `tag_promotion_location`.`active` = 1 
				LEFT JOIN `tag_location` ON `tag_promotion_location`.`location_id` = `tag_location`.`location_id` AND `tag_location`.`active` = 1 
				WHERE ((tag_promotion.start_date <= '.$current_time.' and tag_promotion.end_date >= '.$current_time.') or (tag_promotion.start_date = 0)) and tag_promotion.`active` = 1
				ORDER BY '.$order_by.'
				LIMIT '.$limit.' OFFSET '.$offset
				) );
		$results['count'] = DB::select( DB::raw('SELECT FOUND_ROWS() as count'));
		$results['count'] = $results['count'][0]->count;
		return $results;
	}
}