@extends('layouts.default')
@section('content')
<form class="pure-form">
	<fieldset>
		<div class="pure-control-group">
			<input class="pure-input-1-2" type="text" placeholder="Name">
		</div>
		<div class="clear"></div>
		<div class="pure-control-group">
			<input class="pure-input-1-2" type="text" placeholder="Start Date" id="datepick2">
			<div class="date-icon"><img src="web-images/date-icon.png" width="18" height="18" alt="date-icon" id="datepick2"></div>
		</div>
		
		
		<div class="clear"></div>
		<div class="pure-control-group">
			<input class="pure-input-1-2" type="text" placeholder="End Date" id="datepick3">
			<div class="date-icon"><img src="web-images/date-icon.png" width="18" height="18" alt="date-icon"></div>
		</div>
		
		
		<div class="clear"></div>
		<div class="pure-control-group">
			<textarea id="description" class="pure-input-1-2" placeholder="Description"></textarea>
		</div>
		
		<div class="clear"></div>
		<div class="pure-control-group">
			<textarea id="venue" class="pure-input-1-2" placeholder="Venue"></textarea>
		</div>
		
		<div class="clear"></div>
		<div class="pure-control-group">
			<div class="google-maps"><img src="web-images/google-map-img.png" alt="google-map-img" class="pure-img img"></div>
			<div class="rotate-icon"><img src="web-images/rotate-icon.png" width="28" height="22" alt="icn-rotate" class="pure-img rotate-icon"></div>
		</div>
		
		
		<div class="clear"></div>
		<div class="pure-control-group">
			<button type="submit" class="pure-button pure-button-primary">ADD</button>
		</div>

	</fieldset>
</form>
@stop


