<?php
/*
 * This library will be used to create the common functions between service and web app related to the promotion section
 */
Class PromotionLib {

	/*
	 * This function will create the list of promotions
	 */
	public function get_promotion_list($lat=0,$lon=0,$limit=0,$offset=0,$order_by="distance")
	{
		$promotion_model = new Promotion();
		$promotions = $promotion_model->list_promotions($lat,$lon,$limit,$offset,$order_by);
		// increase the offset with the count of the promotion list
		$offset = $offset+count($promotions['records']);
		
		// check the availability of more records for the request
		if($offset>=$promotions['count']|| $limit!=count($promotions['records']))
		{
			$offset = 0;
		}
		$promotions['offset']=$offset;
		return $promotions;
	}
	
	/*
	 * This function will create the promotion element for the promotion list
	 */
	public function create_promotion_element($promotion) 
	{
		$result = array(
				"promotion_id"=> $promotion->promotion_id,
				"title"=> $promotion->title,
				"description"=> $promotion->discription,
				"image_url"=> Config::get("app.media_url").md5($promotion->user_id)."/".$promotion->image,//$promotion->image,
				"distance"=> $this->distance_formater($promotion->distance),//"28 ft away",
				"paid"=> $promotion->paid
		);
		return $result;
	}
	
	/* 
	 * This function will calculate the distance in feet if less than one km
	 * The distance variable should be in Km
	 */
	public function distance_formater($distance)
	{
		if($distance>=1)
		{
			return round ( $distance ,0, PHP_ROUND_HALF_UP )." km away";
		}
		else if($distance==0)
		{
			return "";
		}
		else 
		{
			// convert the distance to feat from km
			return round ( ($distance*3280.84) ,0, PHP_ROUND_HALF_UP )." ft away";
		}
	}
}