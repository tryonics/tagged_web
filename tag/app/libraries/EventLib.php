<?php
/*
 * This library will be used to create the common functions between service and web app related to the event section
 */
Class EventLib {
	
	/*
	 * The function will list the events with limit offset
	 */
	public function get_event_list($limit,$offset)
	{
		$result=array();
		$event = new EventModel();
		$event_list = $event->get_event_list($limit,$offset);
		$total_event_list = $event->get_event_list();
		$result['records'] = $event_list;
		
		// increase the offset with the count of the promotion list
		$offset = $offset+count($event_list);
		
		// check the availability of more records for the request
		if($offset>=count($total_event_list)|| $limit!=count($event_list))
		{
			$offset = 0;
		}
		
		$result['offset'] = $offset;
		return $result;
	}
	
	/*
	 * This function will create the event element for the event list
	*/
	public function create_event_element($event)
	{
		$result = array(
				"event_id" => $event->event_id,
				"day" => date('d',$event->start_date),
				"title" => $event->event_title,
				"date" => date('l, M d, Y',$event->start_date), //"Friday, June 27, 2014",
				"time" => date('g:i A',$event->start_date),
				"place" => $event->venu,
		);
		return $result;
	}
}