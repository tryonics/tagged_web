<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBtDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bt_devices', function(Blueprint $table)
		{
			$table->integer('bt_devices_id', true);
			$table->string('mac_id', 256);
			$table->string('device_name', 256);
			$table->integer('customer_id')->comment('This will be the user ID of perticular location');
			$table->integer('item_id');
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bt_devices');
	}

}
