<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->integer('user_id',true);
			$table->integer('user_type')->comment('1- user, 2- customer');
			$table->string('first_name', 256);
			$table->string('last_name', 256);
			$table->string('email', 256);
			$table->string('password', 256);
			$table->integer('phone')->nullable();
			$table->integer('fax')->nullable();
			$table->string('activation_code', 256);
			$table->string('forgotten_password_code', 256);
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->integer('last_login')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
