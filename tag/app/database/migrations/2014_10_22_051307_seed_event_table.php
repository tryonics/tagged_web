<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('event')->insert(
			array(
				array(
					'user_id' => 1,
					'event_title' => 'Real Women Have Curves ',
					'event_description' => "Meet Gracie Bowen, she's your average, ordinary 15-year-old girl, except for one thing: she's determined to play varsity soccer... on the boys' team! But when her school forbids her to play and even her family questions her ability, Gracie sets out",
					'image' => "http://www.event.lk/images/event/home/main/1412954326Real%20Women%20Have%20Curves.jpg",
					'start_date' => "1414215265",
					'end_date' => "1414215265",
					'lat' =>'7.109410',
					'lon' =>'79.857968',
					'venu' => "American Center",
					'active' =>1,
					'created_at' => time()
				),
				array(
					'user_id' => 1,
					'event_title' => ' Designer Wedding Show 2014 Designer Wedding Show 2014',
					'event_description' => "Featuring designer creations by:
Aslam Hussein
Cheryl Gooneratne
Dhananjaya Bandara
Eric Suriyasena
Geetha Boderagama
Hameedia
Haris Wijesinghe
Indeevari Yapa Abeywardena
Michael Wijesuriya",
					'image' => "http://www.event.lk/images/event/home/main/1410917614Designer%20Wedding%20Show%202014.jpg",
					'start_date' => "1414475600",
					'end_date' => "1414475600",
					'lat' =>'7.109410',
					'lon' =>'79.857968',
					'venu' => "American Center",
					'active' =>1,
					'created_at' => time()
				),
				array(
					'user_id' => 3,
					'event_title' => 'Sihina Horu Aran',
					'event_description' => "Jayani Senanayake , Priyankara Ratnayake , Gihan fernando ,Dharmapriya dias , Shyam Fernando , Sampath Jayaweera , sampath jayaweera . Dayadewa jayasignhe , Prasad suriyaarachchi",
					'image' => "http://www.event.lk/images/event/home/main/1413006311Sihina%20Horuaran%20copy.jpg",
					'start_date' => " 1414216400",
					'end_date' => " 1414216400",
					'lat' =>'7.109410',
					'lon' =>'79.857968',
					'venu' => "American Center",
					'active' =>1,
					'created_at' => time()
				),
				array(
					'user_id' => 2,
					'event_title' => '2014 Lumbini Walk ',
					'event_description' => "2014 Lumbini walk is planned to held in 25th octomber",
					'image' => "www.event.lk/images/event/home/main/14129930462014 Lumbini Walk.jpg",
					'start_date' => "1414215265",
					'end_date' => "1414215265",
					'lat' =>'7.109410',
					'lon' =>'79.857968',
					'venu' => "American Center",
					'active' =>1,
					'created_at' => time()
				),
				
				array(
						'user_id' => 2,
						'event_title' => '2014 Dharma raja Walk ',
						'event_description' => "2014 Lumbini walk is planned to held in 25th octomber",
						'image' => "www.event.lk/images/event/home/main/14129930462014 Lumbini Walk.jpg",
						'start_date' => "1414215265",
						'end_date' => "1414215265",
						'lat' =>'7.109410',
						'lon' =>'79.857968',
						'venu' => "American Center",
						'active' =>1,
						'created_at' => time()
				),
				
				array(
						'user_id' => 2,
						'event_title' => '2014 Trinity Walk ',
						'event_description' => "2014 Lumbini walk is planned to held in 25th octomber",
						'image' => "www.event.lk/images/event/home/main/14129930462014 Lumbini Walk.jpg",
						'start_date' => "1414215265",
						'end_date' => "1414215265",
						'lat' =>'7.109410',
						'lon' =>'79.857968',
						'venu' => "American Center",
						'active' =>1,
						'created_at' => time()
				),
				
				array(
						'user_id' => 2,
						'event_title' => '2014 Ananda Walk ',
						'event_description' => "2014 Lumbini walk is planned to held in 25th octomber",
						'image' => "www.event.lk/images/event/home/main/14129930462014 Lumbini Walk.jpg",
						'start_date' => "1414215265",
						'end_date' => "1414215265",
						'lat' =>'7.109410',
						'lon' =>'79.857968',
						'venu' => "American Center",
						'active' =>1,
						'created_at' => time()
				),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
