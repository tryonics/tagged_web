<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('location')->insert(
			array(
				array(
					"user_id" => 1,
					"business_name" => "Colombo business1",
					"description" => "This is a test business of Colombo",
					"image" => "http://www.colomboguide.net/images/banners/movie_directory_banner.jpg",
					"lat" => "6.927079",
					"lon" => "79.861243",
					"active" =>1,
					"parent_location" =>1,
				),
				array(
						"user_id" => 1,
						"business_name" => "Colombo business2",
						"description" => "This is a test business of Colombo",
						"image" => "http://www.colomboguide.net/images/banners/movie_directory_banner.jpg",
						"lat" => "6.927276",
						"lon" => "79.851508",
						"active" =>1,
						"parent_location" =>0,
				),
				array(
						"user_id" => 2,
						"business_name" => "Trinko business",
						"description" => "This is a test business of Colombo",
						"image" => "http://www.colomboguide.net/images/banners/movie_directory_banner.jpg",
						"lat" => "6.927079",
						"lon" => "79.861243",
						"active" =>1,
						"parent_location" =>1,
				),
				array(
						"user_id" => 3,
					    "business_name" => "Anuradhapura business",
						"description" => "This is a test business of Colombo",
						"image" => "http://www.colomboguide.net/images/banners/movie_directory_banner.jpg",
						"lat" => "6.927079",
						"lon" => "79.861243",
						"active" =>1,
						"parent_location" =>1,
				),
				
				array(
						"user_id" => 4,
						"business_name" => "Jaffna business",
						"description" => "This is a test business of Colombo",
						"image" => "http://www.colomboguide.net/images/banners/movie_directory_banner.jpg",
						"lat" => "6.927079",
						"lon" => "79.861243",
						"active" =>1,
						"parent_location" =>1,
				),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
