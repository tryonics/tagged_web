<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedPromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('promotion')->insert(
			array(
				array(
					'title'=> "Stand a chance to win Rs.500 worth Gift pack from Knorr! Fifty winners to be selected",
					'discription'=> "Vouchers are redeemable at wow.lk Head Office (Digital Commerce Lanka (PVT) Ltd) No. 176/1, Thimbirigasyaya Road, Colombo 05
Voucher holders must produce the voucher at the point of redemption.",
					'short_description' => "Voucher is valid from 16th October 2014 to 25th October 2014.",
					'start_date' =>"1413366845",
					'end_date' =>1414662845,
					'user_id' =>1,
					'image' => "http://cdn.wow.lk/var/gen/atg/2014/dailydeals/knorrgiftpackoct14/static.jpg",
					'active' =>1,
				),
				array(
					'title'=> " 21% OFF! Choose from a 7Kg or 8.5Kg LG Top Loading Automatic Washing Machine for just Rs.47,650 onwards! ",
					'discription'=> "Vouchers are redeemable at wow.lk Head Office (Digital Commerce Lanka (PVT) Ltd) No. 176/1, Thimbirigasyaya Road, Colombo 05
	Voucher holders must produce the voucher at the point of redemption.",
					'short_description' => "Voucher is valid from 16th October 2014 to 25th October 2014.",
					'start_date' =>"1413366845",
					'end_date' =>1414662845,
					'user_id' =>1,
					'image' => "https://mydeal.lk/deals/CC/2014/2389/LG-washing-machine-01.png",
					'active' =>1,
				),
				array(
					'title'=> "40% OFF! 3 in 1 Cami Shaper",
					'discription'=> "Vouchers are redeemable at wow.lk Head Office (Digital Commerce Lanka (PVT) Ltd) No. 176/1, Thimbirigasyaya Road, Colombo 05
	Voucher holders must produce the voucher at the point of redemption.",
					'short_description' => "Voucher is valid from 16th October 2014 to 25th October 2014.",
					'start_date' =>"1413366845",
					'end_date' =>"1414662845",
					'user_id' =>1,
					'image' => "https://mydeal.lk/deals/CC/2014/2387/Cami-Shapes-01.jpg",
					'active' =>1,
				),
				array(
					'title'=> " 40% OFF! M-works 70",
					'discription'=> "Vouchers are redeemable at wow.lk Head Office (Digital Commerce Lanka (PVT) Ltd) No. 176/1, Thimbirigasyaya Road, Colombo 05
	Voucher holders must produce the voucher at the point of redemption.",
					'short_description' => "Voucher is valid from 16th October 2014 to 25th October 2014.",
					'start_date' =>"1413366845",
					'end_date' =>1414662845,
					'user_id' =>1,
					'image' => "https://mydeal.lk/deals/CC/2014/2383/tripod-projector-screen-01.png",
					'active' =>1,
				),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
