<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event', function(Blueprint $table)
		{
			$table->integer('event_id', true);
			$table->integer('user_id');
			$table->string('event_title', 256);
			$table->text('event_description');
			$table->string('image', 256);
			$table->integer('start_date');
			$table->integer('end_date');
			$table->double('lat');
			$table->double('lon');
			$table->string('venu', 256);
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event');
	}

}
