<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotion_location', function(Blueprint $table)
		{
			$table->integer('promotion_location_id', true);
			$table->integer('promotion_id');
			$table->integer('location_id');
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotion_location');
	}

}
