<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location', function(Blueprint $table)
		{
			$table->integer('location_id', true);
			$table->integer('user_id');
			$table->string('business_name', 256);
			$table->text('description');
			$table->string('image', 256)->nullable();
			$table->integer('country_id');
			$table->integer('city_id')->nullable();
			$table->integer('state_id')->nullable();
			$table->string('address', 256);
			$table->string('lat', 256);
			$table->string('lon', 256);
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->integer('parent_location')->comment('1- the location of the main place, 0- sub location');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location');
	}

}
