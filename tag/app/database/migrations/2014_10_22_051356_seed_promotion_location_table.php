<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedPromotionLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('promotion_location')->insert(
			array(
				array(
					'promotion_id' =>1,
					'location_id'  =>1,
					'active'	   =>1
				),
				array(
					'promotion_id' =>2,
					'location_id'  =>3,
					'active'	   =>1
				),
				array(
						'promotion_id' =>3,
						'location_id'  =>4,
						'active'	   =>1
				),
				array(
						'promotion_id' =>4,
						'location_id'  =>5,
						'active'	   =>1
				),
				array(
						'promotion_id' =>1,
						'location_id'  =>2,
						'active'	   =>1
				),
				
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
