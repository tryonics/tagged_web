<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotion', function(Blueprint $table)
		{
			$table->integer('promotion_id', true);
			$table->string('title', 256);
			$table->text('discription');
			$table->text('short_description');
			$table->integer('start_date');
			$table->integer('end_date');
			$table->integer('user_id');
			$table->string('image', 256);
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->integer('paid')->comment('1- paid, 0- normal add');
			$table->integer('UpdatedBy');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotion');
	}

}
