<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_content', function(Blueprint $table)
		{
			$table->integer('sub_content_id', true);
			$table->integer('item_id');
			$table->string('image', 256);
			$table->string('title', 256);
			$table->text('description');
			$table->integer('active')->comment('1- Active, 0- Pending/dissable, 2- Delete');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_content');
	}

}
