<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('user')->insert(
			array(
				array(
					'user_type' => 2,
					'first_name' => 'Lucil',
					'last_name' => 'Sandaruwan',
					'email' => 'lucilsandaruwan@gmail.com',
					'active' => 1
				),
				array(
						'user_type' => 2,
						'first_name' => 'Dimal',
						'last_name' => 'Govinna',
						'email' => 'dimal@gmail.com',
						'active' => 1
				),
				array(
						'user_type' => 2,
						'first_name' => 'Sankalpana',
						'last_name' => 'Karunarathna',
						'email' => 'sankalpana@gmail.com',
						'active' => 1
				),
				array(
						'user_type' => 2,
						'first_name' => 'Ruwan',
						'last_name' => 'Sandaruwan',
						'email' => 'ruwan@gmail.com',
						'active' => 1
				),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
