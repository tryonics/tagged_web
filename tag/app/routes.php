<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::group(array('domain' => 'www.api.tag.loc','prefix' => 'v1'), function()
Route::group(array('prefix' => 'v1'), function()
{
	Route::get('promotion/list', 'Api_V1_PromotionController@index');
	Route::get('promotion/show/{id}', 'Api_V1_PromotionController@show');
	Route::get('event/list', 'Api_V1_EventController@index');
    Route::resource('promotion', 'Api_V1_PromotionController');
    Route::resource('event', 'Api_V1_EventController');
    
});

//Route::group(array('domain' => 'www.tag.loc','prefix' => ''), function()
Route::group(array('prefix' => 'app'), function()
{
	Route::get('auth/index', 'App_AuthController@index');
	Route::resource('auth', 'App_AuthController');
	Route::get('home/login', 'App_HomeController@login');
	Route::get('home/add_event', 'App_HomeController@add_event');
	Route::get('home/signup', 'App_HomeController@signup');
	Route::post('home/signup', 'App_HomeController@signup');
	Route::resource('home', 'App_HomeController');
});

